import { Injectable } from '@angular/core';

@Injectable()
export class MenuService {

  constructor() { }

  getMenu(): Array<any> {
    const menu = [
      {
        name: 'Inicio',
        path: './inicio',
        children: [
          {
            name: 'Menu',
            path: './menu',
            children: []
          },
          {
            name: 'Clientes',
            path: './clientes',
            children: []
          }
        ]
      }
    ];

    return menu;
  }

}
