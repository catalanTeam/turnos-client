import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
 })
export class ErrorService {

  constructor(
    private toastr: ToastrService) {
  }

  manageError(response) {

    let tipo;
    let titulo;
    let mensaje;

    if(response.status === 400) {//400 Bad Request	The server did not understand the request.
      tipo = 'ERROR';
      titulo = 'Error de validación';
      mensaje = response.error.detalles[0];
    } else if(response.status === 401) {//401 Unauthorized	The requested page needs a username and a password.
      tipo = 'ERROR';
      titulo = 'No autorizado.';
      mensaje = 'No tiene autorización para esta acción.';
    } else if(response.status === 404) {//404 Not Found
      tipo = 'ERROR';
      titulo = response.error.mensaje;
      mensaje = response.error.detalles;
    } else if(response.status === 500) {//500 Internal Server Error	The request was not completed. The server met an unexpected condition.
      tipo = 'ERROR';
      titulo = 'Error en sistema.';
      mensaje = 'Vuelva a intentar más tarde';
    } else {
      tipo = 'ERROR';
      titulo = 'Error en sistema';
      mensaje = 'Vuelva a intentar más tarde';
    }

    if(titulo == undefined || titulo == null) {
      titulo = 'Error en sistema.';
    }

    if(mensaje == undefined || mensaje == null) {
      mensaje = 'Vuelva a intentar más tarde';
    }

    switch (tipo) {
      case 'ERROR':
        this.toastr.error(mensaje, titulo);
      break;
      case 'WARNING':
        this.toastr.warning(mensaje, titulo);
      break;
      case 'INFO':
        this.toastr.info(mensaje, titulo);
      break;
      case 'SUCCESS':
        this.toastr.success(mensaje, titulo);
      break;
    }

  }
}
