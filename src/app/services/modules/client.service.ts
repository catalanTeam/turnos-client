import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';

import { environment } from 'src/environments/environment';
import { Client } from '../../models/class/client';
import { ToastrService } from 'ngx-toastr';
import { catchError } from 'rxjs/operators';
import { ErrorService } from '../config/error.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  baseUrl = environment.baseUrl;
  clientesUrl = this.baseUrl+"/clients";  // URL to web api
  //clientesUrl = "assets/json/clients.json"; // MOCK to json

  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    private error: ErrorService) {
  }

  getClientes(): Observable<HttpResponse<Client[]>> {
    return this.http.get<Client[]>(
      this.clientesUrl, { observe: 'response' })
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.log('HTTP Error');
          this.error.manageError(error);
          return throwError(error);
        })
      );
  }

  createCliente(cliente: Client): Observable<Client> {
    return this.http.post<Client>(this.clientesUrl, cliente, httpOptions)
      .pipe(
        catchError((error) => {
          console.log('HTTP Error');
          this.error.manageError(error);
          return throwError(error);
        })
      );
  }

  updateCliente(cliente: Client): Observable<Client> {
    const url = `${this.clientesUrl}/${cliente.id}`;
    return this.http.put<Client>(url, cliente, httpOptions)
      .pipe(
        catchError((error) => {
          console.log('HTTP Error');
          this.error.manageError(error);
          return throwError(error);
        })
      );
  }

  deleteCliente(id: number): Observable<{}> {
    const url = `${this.clientesUrl}/${id}`;
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.log('HTTP Error');
          this.error.manageError(error);
          return throwError(error);
        })
      );
  }

}
