import { UserLogin } from './userlogin';

export class Login {
  user?: UserLogin;
  token?: string;
}
