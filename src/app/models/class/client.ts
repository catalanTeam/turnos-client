export class Client {
  id: number;
  name: string;
  lastName: string;
  dni: string;
  gender: string;
  phoneNumber: string;
  mail: string;
  address: string;
  birthDate: string;
  observations: string;
}
