import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Login } from '../models/class/login';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private userLoggedSubject: BehaviorSubject<Login>;
    public userLogged: Observable<Login>;

    constructor(private http: HttpClient) {
      this.userLoggedSubject = new BehaviorSubject<Login>(JSON.parse(localStorage.getItem('userLogged')));
      this.userLogged = this.userLoggedSubject.asObservable();
    }

    public get userLoggedValue(): Login {
      return this.userLoggedSubject.value;
    }

    login(username: string, password: string) {
      return this.http.post<any>(`${environment.baseUrl}/login`, { username, password })
        .pipe(map(data => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('userLogged', JSON.stringify(data));
            this.userLoggedSubject.next(data);
            return data;
        }));
    }

    logout() {
      // remove user from local storage to log user out
      localStorage.removeItem('userLogged');
      this.userLoggedSubject.next(null);
    }
}
