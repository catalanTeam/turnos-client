import { Component, OnInit, ElementRef, SimpleChanges } from '@angular/core';
import { Client } from 'src/app/models/class/client';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-clientes-form',
  templateUrl: './clientes-form.component.html',
  styleUrls: ['./clientes-form.component.scss']
})
export class ClientesFormComponent implements OnInit {
  action: Subject<any> = new Subject();
  modo: string;
  readOnly: boolean;
  cliente: Client;
  form: FormGroup;
  validators: any;
  sexoList: any[];

  constructor(
    private ref: ElementRef,
    public modalRef: MDBModalRef,
    private toastr: ToastrService
    ) {
      this.form = new FormGroup({
        id : new FormControl(),
        mail: new FormControl('', [Validators.required, Validators.email]),
        name: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]),
        lastName: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]),
        dni: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(5)]),
        gender: new FormControl('', [Validators.required, Validators.pattern('(M|F)')]),
        birthDate: new FormControl('', [Validators.required]),
        address: new FormControl('', []),
        phoneNumber: new FormControl('', [Validators.pattern('[0-9 \\+\\/\\-\\*]*')]),
        observations: new FormControl()
      });
  }

  ngOnInit(): void {

    this.sexoList = [
      { id: 'M', descripcion: 'MASCULINO' },
      { id: 'F', descripcion: 'FEMENINO' }
    ];

    if ( this.modo !== 'ALTA' ) {
      this.form.setValue(this.cliente);
    }
    if ( this.modo === 'CONS' ) {
      this.readOnly = true;
    } else {
      this.readOnly = false;
    }
    console.log(this.modo + ': ' + this.readOnly);

  }

  onSubmit() {
    if ( this.form.valid ){

      const response = {
        data: this.form.value,
        modo: this.modo
      };

      this.action.next(response);

    } else {
      console.log('Campo Invalid');
      this.showControlError();
    }
  }

  get f(){
    return this.form.controls;
  }

  showControlError() {
    const controls = this.form.controls;
    let hasError = false;
    for (const name in controls) {
      if (!hasError && controls[name].invalid) {
        this.toastr.warning('El campo ' + name + ' no cumple con el formato correspondiente', '', {
          positionClass: 'toast-bottom-right'
        })
        .onTap
        .pipe(take(1))
        .subscribe(() => {
          this.ref.nativeElement.querySelector('input.ng-invalid').focus();
        });
        this.ref.nativeElement.querySelector('input.ng-invalid').focus();
        hasError = true;
      }
    }
  }

}
0
