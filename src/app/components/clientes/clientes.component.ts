import { Component, OnInit, ViewChild } from '@angular/core';
import { MDBModalRef, MDBModalService } from 'angular-bootstrap-md';
import { MatTableDataSource } from '@angular/material/table';
import { Client } from 'src/app/models/class/client';
import { DeleteComponent } from '../modals/delete/delete.component';
import { ClientesService } from 'src/app/services/modules/client.service';
import { MatSort } from '@angular/material/sort';
import { ClientesFormComponent } from './clientes-form/clientes-form.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
  modalRef: MDBModalRef;
  displayedColumns = ['id', 'name', 'dni', 'mail', 'vermas', 'editar', 'eliminar'];
  dataSource = new MatTableDataSource();
  clientes: Client[];
  dataLength: number = 0;
  clienteSelected: number = 0;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private service: ClientesService,
    private modalService: MDBModalService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getClientes();
  }

  getClientes(): void {

    this.service.getClientes().subscribe(
      res => {
        console.log('HTTP response', res);
        this.clientes = res.body;
        this.refreshTable();
      }
    );

  }

  refreshTable(){
    this.dataSource = new MatTableDataSource(this.clientes);
    this.dataSource.sort = this.sort;
    this.setLength();
  }

  setLength() {
    this.dataLength = this.dataSource.filteredData.length;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.setLength();
  }

  openDeleteConfirm(clienteSelected: Client) {

    this.modalRef = this.modalService.show(DeleteComponent, {
      backdrop: true,
      keyboard: false,
      focus: true,
      show: false,
      ignoreBackdropClick: true,
      class: 'modal-dialog-centered modal-sm modal-notify modal-danger',
      containerClass: 'right',
      animated: true
    });

    this.modalRef.content.action.subscribe( (deleteConfirm: any) => {

      this.modalRef.hide();
      console.log(deleteConfirm);
      if ( deleteConfirm ) {
        this.deleteCliente(clienteSelected.id);
      }

    });

  }

  openForm(clienteSelected: Client, modo: string) {

    if ( modo === 'ALTA ') {
      clienteSelected = new Client();
    }

    this.modalRef = this.modalService.show(ClientesFormComponent, {
      backdrop: true,
      keyboard: false,
      focus: true,
      show: false,
      ignoreBackdropClick: true,
      containerClass: 'right',
      animated: true,
      class: 'modal-dialog-centered modal-lg modal-dialog-scrollable',
      data: {
        modo: modo,
        cliente: clienteSelected
      }
    });

    this.modalRef.content.action.subscribe( (response: any) => {

      console.log(response);
      if ( response.modo === 'ALTA' ) {
        console.log('DAR DE ALTA CLIENTE');
        console.log(response.data);
        this.createCliente(response.data, this.modalRef);
      } else {
        console.log('MODIFICAR CLIENTE');
        this.updateCliente(response.data, this.modalRef);
      }

    });

  }

  createCliente(cliente, modal): void {

    this.service.createCliente(cliente).subscribe(
      res => {
        console.log('HTTP response', res);
        this.toastr.success('Cliente creado correctamente.');
        this.clientes.push(res);
        this.refreshTable();
        modal.hide();
      }
    );

  }

  updateCliente(cliente, modal): void {

    this.service.updateCliente(cliente).subscribe(
      res => {
        console.log('HTTP response', res);
        this.toastr.success('Cliente modificado correctamente.');
        const index = this.clientes.findIndex(item => item.id === res.id);
        this.clientes[index] = cliente;
        this.refreshTable();
        modal.hide();
      }
    );

  }

  deleteCliente(id): void {

    this.service.deleteCliente(id).subscribe(
      res => {
        console.log('HTTP response', res);
        this.toastr.success('Cliente eliminado correctamente.');
        this.clientes = this.clientes.filter(item => item.id !== id);
        this.refreshTable();
      }
    );

  }

}
