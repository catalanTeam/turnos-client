import { Component, OnInit, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { take, first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/security/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  returnUrl: string;

  constructor(
    private ref: ElementRef,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService) {

      // redirect to home if already logged in
      if (this.authenticationService.userLoggedValue) {
        this.router.navigate(['/']);
      }

      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

  }

  ngOnInit(): void {

    this.form = new FormGroup({
      user: new FormControl('', [Validators.required]),
      pass: new FormControl('', [Validators.required])
    });

  }

  onSubmit() {
    if ( this.form.valid ){

      this.authenticationService.login(this.f.user.value, this.f.pass.value)
      .pipe(first())
      .subscribe(
        data => {
            this.router.navigate([this.returnUrl]);
        },
        error => {
          //this.error = error;
          this.toastr.error('Usuario y/o Contraseña incorrectos.', '', {
            positionClass: 'toast-bottom-right'
          });
        }
      );

    } else {

      console.log('Campo Invalid');
      const controls = this.form.controls;
      let hasError = false;
      for (const name in controls) {
        if (!hasError && controls[name].invalid) {
          this.toastr.warning('El campo es obligatorio.', '', {
            positionClass: 'toast-bottom-right'
          })
          .onTap
          .pipe(take(1))
          .subscribe(() => {
            this.ref.nativeElement.querySelector('input.ng-invalid').focus();
          });
          this.ref.nativeElement.querySelector('input.ng-invalid').focus();
          hasError = true;
        }
      }

    }
  }

  get f(){
    return this.form.controls;
  }

  isUserValid(login: any) {
    let valido = false;
    if ( login.user === 'acatalan98' && login.pass === '41243707' ) {
      valido = true;
    }
    return valido;
  }

}
