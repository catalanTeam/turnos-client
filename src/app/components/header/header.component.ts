import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from 'src/app/security/authentication.service';
import { Router } from '@angular/router';
import { Login } from 'src/app/models/class/login';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUser: Login;
  username: string;

  @Output() toggleMenu = new EventEmitter();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) {
      this.authenticationService.userLogged.subscribe(x => this.currentUser = x);
    }

  ngOnInit(): void {
    this.username = this.authenticationService.userLoggedValue.user.username;
  }

  logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }

}
