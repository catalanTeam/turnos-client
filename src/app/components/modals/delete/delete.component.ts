import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {
  action: Subject<any> = new Subject();

  constructor(public modalRef: MDBModalRef) { }

  ngOnInit(): void {
  }

  onYesClick() {
    this.action.next(true);
  }

  onNoClick() {
      this.action.next(false);
  }

}
