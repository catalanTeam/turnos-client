import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MDBModalService, MDBModalRef } from 'angular-bootstrap-md';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-custom-table',
  templateUrl: './custom-table.component.html',
  styleUrls: ['./custom-table.component.scss']
})
export class CustomTableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
